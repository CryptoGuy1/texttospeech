import logging
import threading
from gtts import gTTS
import os

logger = logging.getLogger(__name__)


class Talker(threading.Thread):
    def __init__(self, text='please enter the currect arguments', language='en'):
        super(Talker, self).__init__()
        self._text = text
        self._language = language
        self.thread = threading.Thread(target=self.run, args=())
        self.thread.daemon = True
        self.thread.start()

    def run(self):
        logger.debug('Saying: {} in {}'.format(self._text, self._language))
        audio_file_path = "/tmp/audio_tts_speech.mp3"
        tts_speech = gTTS(text=self._text, lang=self._language)
        tts_speech.save(audio_file_path)
        os.system('mpg321 {} -quiet 2>/dev/null &'.format(audio_file_path))
