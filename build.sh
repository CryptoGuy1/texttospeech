#!/bin/bash

project_name="texttospeech"
rm *.deb
rm *.tar.gz
apt-get install python3-stdeb fakeroot python-all build-essential
python3 setup.py --command-packages stdeb.command sdist_dsc --dist-dir=${project_name}_deb bdist_deb
cp preinst ${project_name}_deb/${project_name}-$1/debian/
cd ${project_name}_deb/${project_name}-$1/
dpkg-buildpackage -rfakeroot -uc -us -b
cd ../../
cp ${project_name}_deb/*deb ./
rm -rf ${project_name}_deb/

