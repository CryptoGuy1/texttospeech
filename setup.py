#!/usr/bin/env python3

from setuptools import setup

def readme():
    with open('README') as f:
        return f.read()
setup(
    name='texttospeech',
    version='0.1.3',
    description='text to speech program works with variety of languages',
    long_description=readme(),
    packages=['texttospeech'],
    package_data={'texttospeech': ['assets/*.*']},
    url='',
    python_requires=">=3.6",
    license='',
    zip_safe=False,
    author='Guy Milrud',
    author_email='guymilrud@gmail.com',
    scripts=['bin/tts_milrud'],
    py_modules=['texttospeech'],
    install_requires=['gTTS>=2.0.3']

)
